/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vue;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 *
 * @author carrasco
 */
public class EditorBox extends VBox{
    Label lTitle;
    
    TextField tfTaille;
    Button btReinit;
    
    
    TextField tfProb;
    Button btAlea;
    HBox aleaBox;
    
    public EditorBox() {
        super();
        
        lTitle = new Label("Options de Jeu");
        
        tfTaille = new TextField();       
        tfTaille.setPromptText("Taille du plateau");
        tfTaille.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (!newValue.matches("\\d*")) {
                    tfTaille.setText(newValue.replaceAll("[^\\d]", ""));
                }
            }
        });
        
        btReinit = new Button("Réinitialiser le jeu");
        
        tfProb = new TextField();
        tfProb.setPromptText("Probabilité");
        tfProb.setMaxWidth(100);
        tfProb.setMinWidth(100);

        btAlea = new Button("Aléatoire");
        
        aleaBox = new HBox(tfProb, btAlea);
        
        
        
        
        this.getChildren().addAll(lTitle, tfTaille, btReinit, aleaBox);
        
        this.setStyle("-fx-background-color: lightblue");
        this.setPadding(new Insets(10, 10, 10, 10));
        this.setSpacing(10);
    }
    
    
    /*-----------------------------------------------------
        Getteurs - Setteurs
    -----------------------------------------------------*/
    
    public Label getTitle(){ return this.lTitle; }
    public TextField getTextFieldTaille(){ return this.tfTaille; }
    public TextField getTextFieldProb(){ return this.tfProb; }    
    public Button getBtReinit(){ return this.btReinit; }
    public Button getBtAlea(){ return this.btAlea; }
}
