/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vue;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.layout.BorderPane;

/**
 *
 * @author carrasco
 */
public class SpeedBox extends BorderPane{
    Label lTitle;
    Slider slider;
    Label lLeft;
    Label lRight;
    
    public SpeedBox(){
        super();
        
        lTitle = new Label("Vitesse : 0");
        slider = new Slider(0, 500, 250);
        lLeft = new Label("0");
        lRight = new Label("500");
        
        this.setTop(lTitle);
        this.setCenter(slider);
        this.setLeft(lLeft);
        this.setRight(lRight);
        BorderPane.setAlignment(lTitle, Pos.CENTER);
        BorderPane.setAlignment(slider, Pos.CENTER);
        BorderPane.setAlignment(lLeft, Pos.CENTER);
        BorderPane.setAlignment(lRight, Pos.CENTER);
        
        this.setStyle("-fx-background-color: greenyellow");
        this.setPadding(new Insets(10, 10, 10, 10));
    }
    
    /*-----------------------------------------------------
        Getteurs - Setteurs
    -----------------------------------------------------*/
    
    public Label getTitle(){ return this.lTitle; }
    public Slider getSlider(){ return this.slider; }
    public Label getLabelLeft(){ return this.lLeft; }
    public Label getLabelRight(){ return this.lRight; }
}
