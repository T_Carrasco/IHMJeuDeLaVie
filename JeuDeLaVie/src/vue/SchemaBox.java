/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vue;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

/**
 *
 * @author theo
 */
public class SchemaBox extends VBox{
    Label lTitle;
    Board board;
    Label lDefinition;
    ComboBox combo;
    Button btLoad;
    
    public SchemaBox(){
        super();
        
        lTitle = new Label("Zone Tampon");
        board = new Board(10, 10);
        lDefinition = new Label("Choisissez un schéma :");        
        combo = new ComboBox();
        combo.getItems().addAll("Planeur", "Eboueur");
        btLoad = new Button("Chargez");
        
        lTitle.setAlignment(Pos.CENTER);
        board.setAlignment(Pos.CENTER);
        
        VBox.setMargin(board, new Insets(20,20,20,15));
        
        this.getChildren().addAll(lTitle, board, lDefinition, combo, btLoad);
        this.setStyle("-fx-background-color: lightgreen");
        this.setPadding(new Insets(10, 10, 10, 10));
        this.setSpacing(10);
    }
    
    /*-----------------------------------------------------
        Getteurs - Setteurs
    -----------------------------------------------------*/
    
    public Label getTitle(){ return this.lTitle; }
    
    public Board getBoard(){ return this.board; }
    public Label getDefinition(){ return this.lDefinition; }
    public ComboBox getCombo(){ return this.combo; }    
    public Button getBtLoad(){ return this.btLoad; }
}
