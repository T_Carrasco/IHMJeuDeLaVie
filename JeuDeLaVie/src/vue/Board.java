/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vue;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;

/**
 *
 * @author theo
 */
public class Board extends GridPane{
    int size;
    int scale;
    Label tab[][];
    
    public Board(int size, int scale){
        super();
        
        this.size = size;
        this.scale = scale;
        this.tab = new Label[size][size];
        this.setHgap(1);
        this.setVgap(1);
        
        
        for(int i=0; i<size; i++){
            for(int j=0; j<size; j++){
                Label l = new Label();
                l.setMinSize(scale, scale);
                l.setMaxSize(scale, scale);
                l.setStyle("-fx-background-color: white");
                this.tab[i][j] = l;
                this.add(l, i, j);
            }
        }
        this.setAlignment(Pos.CENTER);
        this.setStyle("-fx-background-color: lightgrey");
    }
    
    public void setColorCell(int x, int y, boolean alive){
        Label l = new Label();
        l.setMinSize(scale, scale);
        l.setMaxSize(scale, scale);

        if(alive)
            l.setStyle("-fx-background-color: black");
        else
            l.setStyle("-fx-background-color: white");
        
        this.getChildren().remove(this.tab[x][y]);
        this.tab[x][y] = l;
        this.add(l, x, y);
    }
}
