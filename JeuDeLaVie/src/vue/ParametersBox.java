/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vue;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;

/**
 *
 * @author carrasco
 */
public class ParametersBox extends VBox{
    Label lTitle;
    
    GridPane grid;
    
    Label lmS;
    Label lmA;
    Label lvMin;
    Label lvMax;
    
    ComboBox cmS;
    ComboBox cmA;
    ComboBox cvMin;
    ComboBox cvMax;
    
    public ParametersBox(){
        super();
        
        lTitle = new Label("Editeur de Jeu");
        
        grid = new GridPane();
        
        lmS = new Label("- Mort Solitude");
        lmA = new Label("- Mort Solitude");
        lvMin = new Label("- Vie Min");
        lvMax = new Label("- Vie Max");
        
        grid.add(lmS, 0, 0);
        grid.add(lmA, 0, 1);
        grid.add(lvMin, 0, 2);
        grid.add(lvMax, 0, 3);
        
        cmS = new ComboBox();
        cmA = new ComboBox();
        cvMin = new ComboBox();
        cvMax = new ComboBox();
        
        cmS.getItems().addAll(1, 2, 3, 4);
        cmA.getItems().addAll(1, 2, 3, 4);
        cvMin.getItems().addAll(1, 2, 3, 4);
        cvMax.getItems().addAll(1, 2, 3, 4);
        
        grid.add(cmS, 1, 0);
        grid.add(cmA, 1, 1);
        grid.add(cvMin, 1, 2);
        grid.add(cvMax, 1, 3);        

        grid.setHgap(5);
        grid.setVgap(5);
        
        this.setAlignment(Pos.CENTER);
        
        this.getChildren().addAll(lTitle, grid);
        
        this.setStyle("-fx-background-color: lightcoral");
        this.setPadding(new Insets(10, 10, 10, 10));
        this.setSpacing(10);
    }
    
    
    /*-----------------------------------------------------
        Getteurs - Setteurs
    -----------------------------------------------------*/
    
    public Label getTitle(){ return this.lTitle; }
    
    public GridPane getGrid(){ return this.grid; }
    
    public Label getLabelMortSolitude(){ return this.lmS; }    
    public Label getLabelMortAsphysie(){ return this.lmA; }    
    public Label getLabelVieMin(){ return this.lvMin; }    
    public Label getLabelVieMax(){ return this.lvMax; }
    
    public ComboBox getComboBoxMortSolitude(){ return this.cmS; }    
    public ComboBox getComboBoxMortAsphysie(){ return this.cmA; }    
    public ComboBox getComboBoxVieMin(){ return this.cvMin; }    
    public ComboBox getComboBoxVieMax(){ return this.cvMax; }      
}
