/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vue;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 *
 * @author carrasco
 */
public class Vue extends BorderPane{
    // Partie Supérieure
    Button btPlay;
    Label lCpt;
    
    HBox topBox;
    
    // Partie Gauche
    EditorBox editorBox;
    ParametersBox parametersBox;
    SpeedBox speedBox;
    Button btReset;
    VBox leftBox;
    
    // Partie Centrale
    Board board;
    
    // Partie Droite
    SchemaBox schemaBox;
    VBox rightBox;
    
    // Partie Basse
    Button btQuit;
    
    
    public Vue() {
        super();
        
        initTop();
        initLeft();
        initRight();
        initCenter();
        initBottom();
        
        
        this.setPadding(new Insets(10, 10, 10, 10));
        this.setStyle("-fx-background-color: ghostwhite;"
                + "");
    }
    
    private void initTop(){
        
        //Image img = new Image("vue/Pause.jpg");
        //ImageView view = new ImageView(img);
        btPlay = new Button("Play");
        btPlay.setMinSize(100, 30);
        //btPlay.setGraphic(view);
        
        lCpt = new Label("Time : 0");
        topBox = new HBox();
        topBox.getChildren().addAll(btPlay, lCpt);
        topBox.setAlignment(Pos.CENTER);
        topBox.setSpacing(25);
        this.setTop(topBox);
    }
    
    private void initLeft(){
        editorBox = new EditorBox();
        parametersBox = new ParametersBox();
        speedBox = new SpeedBox();
        btReset = new Button("Reset");
        
        editorBox.setAlignment(Pos.TOP_CENTER);
        
        leftBox = new VBox();
        leftBox.getChildren().addAll(editorBox, parametersBox, speedBox, btReset);
        leftBox.setAlignment(Pos.CENTER);
        this.setLeft(leftBox);
        this.setAlignment(leftBox, Pos.CENTER);
        leftBox.setSpacing(10);
        leftBox.setPadding(new Insets(10, 10, 10, 10));
    }
    
    private void initRight(){
        schemaBox = new SchemaBox();
        schemaBox.setAlignment(Pos.TOP_CENTER);
        rightBox = new VBox(schemaBox, new Label());
        this.setRight(rightBox);
    }
    
    private void initCenter(){
        board = new Board(100, 5);
        board.setAlignment(Pos.CENTER);
        BorderPane.setMargin(board, new Insets(20, 100, 20, 100));
        this.setCenter(board);
    }
    
    private void initBottom(){
        btQuit = new Button("Quitter");
        this.setAlignment(btQuit, Pos.BOTTOM_RIGHT);
        this.setBottom(btQuit);
    }
    
    
    /*-----------------------------------------------------
        Getteurs - Setteurs
    -----------------------------------------------------*/
    
    public Button getBtPlay(){ return this.btPlay; }
    public Label getlCpt(){ return this.lCpt; }
    
    // Getteurs d'EditorBox
    public Label getEditorTitle(){ return this.editorBox.getTitle(); }
    public TextField getEditorTextFieldTaille(){ return this.editorBox.getTextFieldTaille(); }
    public TextField getEditorTextFieldProb(){ return this.editorBox.getTextFieldProb(); }
    public Button getEditBtReinit(){ return this.editorBox.btReinit; }
    public Button getEditBtAlea(){ return this.editorBox.getBtAlea(); }
    
    // Getteurs de ParametersBox
    public Label getParamTitle(){ return this.parametersBox.getTitle(); }
    public GridPane getParamGrid(){ return this.parametersBox.getGrid(); }
    
    public Label getParamLabMortSolitude(){ return this.parametersBox.getLabelMortSolitude(); }
    public Label getParamLabMortAsphyxie(){ return this.parametersBox.getLabelMortAsphysie(); }
    public Label getParamLabVieMin(){ return this.parametersBox.getLabelVieMin(); }
    public Label getParamLabVieMax(){ return this.parametersBox.getLabelVieMax(); }
    
    public ComboBox getParamComboMortSolitude(){ return this.parametersBox.getComboBoxMortSolitude(); }
    public ComboBox getParamComboMortAsphyxie(){ return this.parametersBox.getComboBoxMortAsphysie(); }
    public ComboBox getParamComboVieMin(){ return this.parametersBox.getComboBoxVieMin(); }
    public ComboBox getParamComboVieMax(){ return this.parametersBox.getComboBoxVieMax(); }

    // Getteurs de SchemaBox
    public Label getSchemaTitle(){ return this.schemaBox.getTitle(); }
    public Board getSchemaBoard(){ return this.schemaBox.getBoard(); }
    public Label getSchemaDefenition(){ return this.schemaBox.getDefinition(); }
    public ComboBox getSchemaCombo(){ return this.schemaBox.getCombo(); }
    public Button getSchemaBtLoad(){ return this.schemaBox.getBtLoad(); }
    
    // Getteurs de SpeedBox
    public Label getSpeedTitle(){ return this.speedBox.getTitle(); }
    public Slider getSpeedSlider(){ return this.speedBox.getSlider(); }
    public Label getSpeedLeft(){ return this.speedBox.getLabelLeft(); }
    public Label getSpeedRight(){ return this.speedBox.getLabelRight(); }
    
    // Getteur du Board
    public Board getBoard(){ return this.board; }
}
